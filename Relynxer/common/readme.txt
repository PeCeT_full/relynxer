Relynxer 1.0.1
Author: PeCeT_full
Website: http://www.pecetfull.pl
Copyright (c) by PeCeT_full 2021-2022. Relynxer is published under The MIT License. For more information, please refer to Licence.txt included with the application.

If there are any problems or doubts, please contact me.

-------------------
Program description
-------------------

Relynxer is a wizard program designed especially for vintage PCs for 'shortening' website URLs by generating new (typically shorter) ones on Lynx.re thanks to its API. The application itself was written in C++ and uses wxWidgets GUI library. The Lynx.re service is based on Shrinky by Hassan.

Minimal hardware and system requirements: 133 MHz or faster processor; 16 MB of available RAM; 3 MB of free hard disk space available where the program exists; Windows 95 or newer operating system; active Internet connection.

-----------------------------
Getting to work in Windows 95
-----------------------------

In order to make the software work properly on your Windows 95 computer, you must have Windows Sockets 2 (Winsock 2) installed there. It is generally required by the network libraries included with the program.

--------------------
Handling the program
--------------------

The program greets you with its wizard window. From there, you are able to enter your valid URL of the website you want to create a shortened link of. After completion, you can always go back to the previous step and create another Lynx.re link without having to restart the application.

------------------
Additional options
------------------

Relynxer, just like Lynx.re, allows you to customise your link by adjusting the following parameters: 

* password, 

* number of usages, 

* expiration date, 

* visibility state (public by default), 

* custom name.

All of these parameters are fully optional.

---------------
Program options
---------------

You can change the UI language and show or hide the HTTP warning on program launch.
