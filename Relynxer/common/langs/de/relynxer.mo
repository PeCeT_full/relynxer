��    =        S   �      8  	   9     C     K     S     W     m     |     �     �  [   �     �     �       6     Y   O  d   �  B        Q     X     i  P   v     �     �     �  
   �     �     �     �               #     A     I     V     ^  	   t  >   ~     �     �     �     �     �     �     	  �   )	  3  �	  �   �     �     �     �     �  .   �  
   �  �   �     �  &   �     �     �     �     �  u    	   }  
   �  	   �     �     �     �  
   �     �     �  h   �  	   W     a     h  7   w  �   �  {   2  K   �  	   �            W   (     �     �     �     �     �     �     �     �     �  %   �  	              .     6  	   O  D   Y     �     �     �     �     �     �  !     �   (    �  �   *     �                  .   .  	   ]  �   g       2   6  (   i     �     �  4   �         $                       =                      9   (   '   <            &   )      :   "                      ,   0   -      5      /              +   
   2       .   	       %   7   3                            6   4   8                         1   *                           #         ;   !    &About... &Cancel &Next > &OK .

Current language:  .
Build date:  < &Back <header> <main message> A wizard program for the link shortening service Lynx.re. Powered by Shrinky.

Build info:  Abort About  Additional options An error has occurred due to the following reason: %s. An error has occurred while processing the request. Enter another URL or try again later. Another language has just been selected. To reflect all the changes, please restart the application. Array of language names and identifiers should have the same size. Cancel Change &language Close window Could not successfully establish the connection due to the following reason: %s. English English (Canada) Error Expire on: Finish German Hide HTTP &warning Language License Lynx.re Link Shortener Wizard Message More options More... Optional custom name: Password: Please wait while a new redirecting link is being generated... Polish Public link Ready to share Relynxer Select your language. Show HTTP &warning Show/hide HTTP &warning This program is published under The MIT License. For more information, please refer to the Licence.txt file included with the application. This program uses HTTP which is not encrypted - this means that data sent through this program can be easily tracked. The encrypted HTTPS protocol is not supported in order to preserve compatibility with legacy Windows versions (especially Windows 95). If you do not wish to have your data possibly compromised by any other party, please do not enter any detail in the wizard and exit Relynxer immediately. The author does not take any responsibility in case of any piece of information retrieved from a request created by this application.

You have been warned. This wizard will guide you through the process of shortening your link.

To continue, please enter the website URL you want to generate a shortened link of: Unknown error. Usages: Warning Welcome to Relynxer You need to enter the URL in order to proceed. Your link: Your shortened link has been successfully generated! Now you can share it with anyone you want to.

To shorten another link, please return to the previous step. banned custom name must be less than 60 chars domain not allowed http://www.pecetfull.pl invalid url spam detected! Project-Id-Version: Relynxer 1.0.1
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2021-11-12 22:47+0100
PO-Revision-Date: 2022-04-18 20:00+0200
Language-Team: 
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Poedit 2.0.5
Last-Translator: PeCeT_full <me@pecetfull.pl>
Plural-Forms: nplurals=2; plural=(n != 1);
Language: de
 &Über... &Abbrechen &Weiter > &OK .

Aktuelle Sprache:  .
Build-Datum:  < &Zurück <Kopfzeile> <Hauptnachricht> Ein Assistentenprogramm für den Linkverkürzer-Service Lynx.re. Unterstützt von Shrinky.

Build-Info:  Abbrechen Über  Zusatzoptionen Ein Fehler ist aus dem folgenden Grund aufgetreten: %s. Bei der Bearbeitung der Anfrage ist ein Fehler aufgetreten. Geben Sie eine weitere URL ein oder versuchen Sie später noch einmal. Eine andere Sprache hat gerade ausgewählt worden. Um alle Änderungen zu übernehmen, starten Sie bitte die Anwendung neu. Das Array von Sprachennamen und Kennungen sollte die gleiche Größe haben. Abbrechen Sprache &ändern Fenster schließen Die Verbindung konnte aus dem folgenden Grund nicht erfolgreich hergestellt werden: %s. Englisch Englisch (Kanada) Fehler Läuft aus am: Fertig stellen Deutsch HTTP-&Warnung ausblenden Sprache Lizenz Assistent für Lynx.re-Linkverkürzer Nachricht Mehr Optionen Mehr... Optionaler eigener Name: Passwort: Bitte warten Sie, bis ein neuer Weiterleitungslink generiert wird... Polnisch Öffentlicher Link Einsatzbereit Relynxer Wählen Sie Ihre Sprache aus. HTTP-&Warnung anzeigen HTTP-&Warnung anzeigen/ausblenden Dieses Programm steht unter der MIT-Lizenz. Weitere Angaben hierzu finden Sie unter Licence.txt-Datei in der Anwendung enthalten. Dieses Programm benutzt HTTP, das nicht verschlüsselt ist - das bedeutet, dass durch dieses Programm übersendeten Daten leicht verfolgt werden können. Das verschlüsselte Protokoll HTTPS wird nicht unterstützt, um die Kompatibilität mit älteren Versionen von Windows (insbesondere Windows 95) zu aufrechtzuerhalten. Wenn Sie Ihre Daten von einer anderen Partei nicht eventuell gefährden wollen, geben Sie bitte keine Einzelheit im Assistenten ein und beenden Sie Relynxer sofort. Der Autor übernimmt keine Verantwortung im Falle von Verschaffen von irgendwelcher Information aus einer Anfrage dieser Anwendung.

Sie wurden gewarnt. Dieser Assistent leitet Sie durch den Prozess der Verkürzung Ihres Links.

Bitte geben Sie die Webseite-URL ein, deren ein gekürzter Link generiert werden sollen, um den Vorgang fortzusetzen: Unbekannter Fehler. Verwendungen: Warnung Willkommen bei Relynxer Sie müssen die URL eingeben, um fortzufahren. Ihr Link: Ihr gekürzter Link wurde erfolgreich generiert! Jetzt können Sie er mit wem Sie wollen teilen.

Um einen anderen Link zu kürzen, kehren Sie bitte zum vorherigen Schritt zurück. Sie werden ausgeschlossen eigener Name muss weniger als 60 Zeichen enthalten die angegebene Domäne ist nicht erlaubt http://www.pecetfull.pl/de ungültige URL die bereitgestellte URL als Spam identifiziert wurde 