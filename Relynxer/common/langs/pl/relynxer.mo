��    =        S   �      8  	   9     C     K     S     W     m     |     �     �  [   �     �     �       6     Y   O  d   �  B        Q     X     i  P   v     �     �     �  
   �     �     �     �               #     A     I     V     ^  	   t  >   ~     �     �     �     �     �     �     	  �   )	  3  �	  �   �     �     �     �     �  .   �  
   �  �   �     �  &   �     �     �     �     �  �       �     �     �     �     �     �  	               u   1     �     �     �  /   �  l   �  v   j  V   �     8     ?     N  P   [  	   �     �     �     �     �  	   �  &   �            )   '  	   Q     [  
   i     t     �  F   �     �     �     �            &   '  $   N    s  �  �  �   )     �     �     �     �  0        =  �   L       2   '  $   Z          �  *   �         $                       =                      9   (   '   <            &   )      :   "                      ,   0   -      5      /              +   
   2       .   	       %   7   3                            6   4   8                         1   *                           #         ;   !    &About... &Cancel &Next > &OK .

Current language:  .
Build date:  < &Back <header> <main message> A wizard program for the link shortening service Lynx.re. Powered by Shrinky.

Build info:  Abort About  Additional options An error has occurred due to the following reason: %s. An error has occurred while processing the request. Enter another URL or try again later. Another language has just been selected. To reflect all the changes, please restart the application. Array of language names and identifiers should have the same size. Cancel Change &language Close window Could not successfully establish the connection due to the following reason: %s. English English (Canada) Error Expire on: Finish German Hide HTTP &warning Language License Lynx.re Link Shortener Wizard Message More options More... Optional custom name: Password: Please wait while a new redirecting link is being generated... Polish Public link Ready to share Relynxer Select your language. Show HTTP &warning Show/hide HTTP &warning This program is published under The MIT License. For more information, please refer to the Licence.txt file included with the application. This program uses HTTP which is not encrypted - this means that data sent through this program can be easily tracked. The encrypted HTTPS protocol is not supported in order to preserve compatibility with legacy Windows versions (especially Windows 95). If you do not wish to have your data possibly compromised by any other party, please do not enter any detail in the wizard and exit Relynxer immediately. The author does not take any responsibility in case of any piece of information retrieved from a request created by this application.

You have been warned. This wizard will guide you through the process of shortening your link.

To continue, please enter the website URL you want to generate a shortened link of: Unknown error. Usages: Warning Welcome to Relynxer You need to enter the URL in order to proceed. Your link: Your shortened link has been successfully generated! Now you can share it with anyone you want to.

To shorten another link, please return to the previous step. banned custom name must be less than 60 chars domain not allowed http://www.pecetfull.pl invalid url spam detected! Project-Id-Version: Relynxer 1.0.1
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2021-11-12 22:47+0100
PO-Revision-Date: 2022-04-18 19:59+0200
Language-Team: 
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Poedit 2.0.5
Last-Translator: PeCeT_full <me@pecetfull.pl>
Plural-Forms: nplurals=3; plural=(n==1 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2);
Language: pl
 O &programie... &Anuluj &Dalej > &OK .

Obecny język:  .
Data kompilacji:  < &Wstecz <nagłówek> <wiadomość główna> Program-kreator dla usługi do skracania łączy Lynx.re. Napędzany przez silnik Shrinky.

Informacje o kompilacji:  Przerwij O programie  Opcje dodatkowe Wystąpił błąd z następującego powodu: %s. Wystąpił błąd podczas przetwarzania żądania. Wprowadź inny adres URL lub spróbuj ponownie później. Właśnie został wybrany inny język. Aby wszelkie zmiany zostały odzwierciedlone, proszę zrestartować aplikację. Tablica nazw języków powinna być tego samego rozmiaru, co tablica identyfikatorów. Anuluj Zmień &język Zamknij okno Nie udało się pomyślnie nawiązać połączenia z następującego powodu: %s. Angielski Angielski (Kanada) Błąd Wygasa dnia: Zakończ Niemiecki Ukrywaj &ostrzeżenie o protokole HTTP Język Licencja Kreator usługi skracania łączy Lynx.re Komunikat Więcej opcji Więcej... Opcjonalna nazwa własna: Hasło: Proszę czekać, trwa generowanie nowego łącza przekierowującego... Polski Łącze publiczne Gotowe do użycia Relynxer Wybierz swój język. Pokazuj &ostrzeżenie o protokole HTTP Pokazuj/ukrywaj &ostrzeżenie o HTTP Ten program jest publikowany na zasadach licencji MIT. Więcej informacji można uzyskać zapoznając się z plikiem Licence.txt dołączonym do aplikacji (polskie tłumaczenie licencji MIT: http://blaszyk-jarosinski.pl/wp-content/uploads/2008/05/licencja-mit-tlumaczenie.pdf). Program ten korzysta z protokołu HTTP, który nie jest szyfrowany - oznacza to, że dane przesłane za pośrednictwem tego programu mogą być łatwo śledzone. Szyfrowany protokół HTTPS nie jest obsługiwany w celu zachowania kompatybilności ze starszymi wersjami systemu Windows (w szczególności Windows 95). Jeżeli nie chcesz, aby Twoje dane zostały ewentualnie naruszone przez jakąkolwiek stronę, proszę nie wprowadzać żadnego szczegółu w kreatorze oraz natychmiast wyjść z programu Relynxer. Autor nie ponosi żadnej odpowiedzialności w przypadku pozyskania dowolnej informacji z żądania utworzonego przez tę aplikację.

Zostałeś ostrzeżony. Ten kreator przeprowadzi Cię przez proces skracania łącza.

Aby kontynuować, proszę wpisać adres URL witryny, którego skrócone łącze ma zostać wygenerowane: Nieznany błąd. Użycia: Ostrzeżenie Witamy w Relynxerze Aby kontynuować, należy wprowadzić adres URL. Twoje łącze: Twoje skrócone łącze zostało pomyślnie wygenerowane! Można go teraz udostępniać dowolnie wybranym przez Ciebie osobom.

Aby skrócić kolejne łącze, proszę wrócić do poprzedniego kroku. jesteś zablokowany nazwa własna musi zawierać mniej niż 60 znaków określona domena nie jest dozwolona http://www.pecetfull.pl/pl nieprawidłowy adres URL podany adres URL został wykryty jako spam 